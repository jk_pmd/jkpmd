package com.precisionmedcare.jkkj.service;

import com.precisionmedcare.jkkj.domain.Iccard;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface UploadService {
    Object insertResultGroup(Map model, String ipAddress, HttpSession session, String IMAGE_PATH) throws IOException;

    void updateResult(Map model,HttpSession session);

    void updateResGroup(String id,String text);

    List getDoctoryText(String id);

    List getAllResGroup(String idArr);

    List downloadGetAllResGroup(HttpSession session);

    List getAllResGroupLog(String idArr);

    List downloadGetAllResGroupLog(HttpSession session);

    List getAllResGroupLogByUserId(String idArr);

    List getAllResGroupByUserId(String idArr);

    List getResultGroupData(HttpSession session,String id,String field,String order,String username);

    List selectRes(String text);

    String[]  getResultGro(HttpSession session,String id);

    Map<String, Object> getPath(HttpSession session, String id);

    int checkPatient(HttpSession session);

    int checkIC(String ic);

    int saveTempNumber(String number,String tempNumber);

    List<Iccard> selectCount(String ic);

    List getReGroup(String id);

    List downloadGetReGroup(String id);

    List getGroup(String id);

    List getActivityCode(String id);

    void delResultGroup(String id);

}
