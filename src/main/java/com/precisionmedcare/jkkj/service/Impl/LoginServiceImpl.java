package com.precisionmedcare.jkkj.service.Impl;

import com.precisionmedcare.jkkj.domain.Iccard;
import com.precisionmedcare.jkkj.domain.Logincode;
import com.precisionmedcare.jkkj.domain.Loginuser;
import com.precisionmedcare.jkkj.mapper.LoginMapper;
import com.precisionmedcare.jkkj.service.LoginService;
import com.precisionmedcare.jkkj.utils.Result;
import com.precisionmedcare.jkkj.utils.dateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Service
public class LoginServiceImpl implements LoginService {
    @Autowired
    private LoginMapper loginMapper;
    @Override

    public List login(String username, String password) {
        List list = loginMapper.login(username, password);
        return list;
    }

    @Override
    public  List<Loginuser> ClientLogin(String username, String password) {
        List<Loginuser> loginusers = loginMapper.ClientLogin(username, password);
        return loginusers;
    }

    @Override
    public void insertLoginLog(String username, HttpSession session, Result result, String loginType, String userid) {
        String ymdhms = dateUtil.get24HCurrentTime_Calendar("ymdhms");
        loginMapper.insertLoginlog(loginType, userid, ymdhms);
        session.setAttribute("username", username);
    }

    @Override
    public void insertLogin(String username, String password, String group, String remark, String endTime,String code, String type) {
        Date date = dateUtil.stringToData(endTime);
        Loginuser loginuser = new Loginuser();
        if (username != null && !"".equals(username)) {
            loginuser.setUserName(username);
        }
        if (date != null) {
            loginuser.setEndTime(date);
        }
        loginuser.setIsDel((byte) 0);
        if (password != null && !"".equals(password)) {
            loginuser.setPwd(password);
        }
        if (remark != null && !"".equals(remark)) {
            loginuser.setRemarks(remark);
        }
        if (group != null && !"".equals(group)) {
            loginuser.setUgroup(group);
        }
        if ("".equals(type)) {
            loginuser.setType(1);
        }else {
            loginuser.setType(Long.parseLong(type));
        }
        loginMapper.insertLoginUser(loginuser);
        long userid = loginuser.getId();
        loginMapper.insertLoginCode(userid,code);
    }

    @Override
    public List checkPwd(String username, String oldPwd ,String type) {
        List list = loginMapper.checkPwd(username, oldPwd ,type);
        return list;
    }

    @Override
    public void insertPwd(String id, String newPwd) {
        loginMapper.updatePwd(id, newPwd);
    }

    @Override
    public void insertLoginCode() {
        Logincode logincode = new Logincode();
        String[] moreUUID = dateUtil.getMoreUUID(5);
        for (int i = 0; i < moreUUID.length; i++) {
            logincode.setStatus(1);
            String code = moreUUID[i];
            logincode.setActivecode(code);
            loginMapper.insetLoginCode(logincode);
        }
    }

    @Override
    public List checkCode(String code) {
        List list = loginMapper.checkCode(code);
        return list;
    }

    @Override
    public List checkUsername(String name) {
        List list = loginMapper.checkUsername(name);
        return list;
    }

    @Override
    public void updateActiveCode(String name, String newcode) {
        loginMapper.updateStatus(name);
        loginMapper.updateActiveCode(name,newcode);
    }

    @Override
    public List checkNewCode(String newCode) {
        List list = loginMapper.checkNewCode(newCode);
        return list;
    }

    @Override
    public List<HashMap> loginManage(String name, String pwd) {
        List<HashMap> hashMaps = loginMapper.selectManage(name, pwd);
        return hashMaps;
    }

    @Override
    public List getUsers(String id) {
        List list = loginMapper.getUsers(id);
        return list;
    }

    @Override
    public List selectByUser(String user) {
        List list = loginMapper.selectByUser(user);
        return list;
    }

    @Override
    public void deleteUser(String userId) {
        loginMapper.deleteUser(userId);
    }

    @Override
    public int selectIC(String number) {
        int i = loginMapper.selectIC(number);
        return i;
    }

    @Override
    public void saveIC(String number) {
        Iccard iccard = new Iccard();
        iccard.setNumber(number);
        loginMapper.saveIC(iccard);
    }

}
