package com.precisionmedcare.jkkj.service.Impl;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.precisionmedcare.jkkj.domain.Resultgroup;
import com.precisionmedcare.jkkj.domain.Resultlog;
import com.precisionmedcare.jkkj.mapper.UploadSliceMapper;
import com.precisionmedcare.jkkj.service.UploadSliceService;
import com.precisionmedcare.jkkj.utils.AlgoUdp;
import com.precisionmedcare.jkkj.utils.dateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.util.Map;

@Service
public class UploadSliceServiceImpl implements UploadSliceService {
    @Autowired
    private UploadSliceMapper uploadSliceMapper;

    @Override
    public Object insertBLKResultGroup(Map model, String ipAddress, HttpSession session, String IMAGE_PATH, String timeStamp) {
        JSONObject object = null;
        if (!model.isEmpty()) {
            String username = model.get("username").toString();
            String patientID = model.get("patientID").toString();
            String detectionID = model.get("detectionID").toString();
            String city = model.get("city") == null ? "" : model.get("city").toString();
            String upTime = dateUtil.get24HCurrentTime_Calendar("ymdhms");
            String date = dateUtil.get24HCurrentTime_Calendar("ymd");
            Resultgroup resultgroup = new Resultgroup();
            resultgroup.setUpTime(upTime);
            resultgroup.setDetectionId(detectionID);
            resultgroup.setUpUser(username);
            resultgroup.setPatientId(patientID);
            resultgroup.setCity(city);
            resultgroup.setDate(date);
            resultgroup.setIp(ipAddress);
            uploadSliceMapper.insertBLKResultGroup(resultgroup);
            long resultgroupId = resultgroup.getId();
            String fileList = model.get("fileList").toString();
            String imgPath = IMAGE_PATH + File.separator + session.getAttribute("username") + File.separator + timeStamp + File.separator + fileList;
            Resultlog resultlog = new Resultlog();
            resultlog.setUpUser(username);
            resultlog.setUpTime(upTime);
            resultlog.setDetectionId(detectionID);
            resultlog.setPatientId(patientID);
            resultlog.setGroupId(resultgroupId);
            resultlog.setImgPath(imgPath);
            uploadSliceMapper.insertBLKResultLog(resultlog);

            object = JSONUtil.createObj();
            object.put("arg01", resultgroupId);
            object.put("patient_id", patientID);
            object.put("detect_id", detectionID);
            object.put("list", imgPath);
            object.put("username", username);
            String udpBLKAlgo = AlgoUdp.getUdpBLKAlgo(object);
            object = JSONUtil.parseObj(udpBLKAlgo);
            /*object = JSONUtil.createObj();
            object.put("arg01", resultgroupId);
            object.put("patient_id", patientID);
            object.put("detect_id", detectionID);
            object.put("last", "");
            object.put("result", "f1(98%)");
            object.put("error", "");*/
        }
        return object;
    }
}
