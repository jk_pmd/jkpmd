package com.precisionmedcare.jkkj.service.Impl;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.precisionmedcare.jkkj.domain.*;
import com.precisionmedcare.jkkj.mapper.UploadMapper;
import com.precisionmedcare.jkkj.service.UploadService;
import com.precisionmedcare.jkkj.utils.AlgoUdp;
import com.precisionmedcare.jkkj.utils.UploadUtils;
import com.precisionmedcare.jkkj.utils.dateUtil;
import com.precisionmedcare.jkkj.utils.excutePython;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.management.ObjectName;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UploadServiceImpl implements UploadService {
    @Autowired
    private UploadMapper uploadMapper;
    @Override
    public Object insertResultGroup(Map model, String ipAddress, HttpSession session,String IMAGE_PATH) throws IOException {
        List list = new ArrayList();
        JSONObject object = null;
        if (!model.isEmpty()) {
            String username = model.get("username").toString();
            String patientID = model.get("patientID").toString();
            String detectionID = model.get("detectionID").toString();
            String heartbeat = model.get("heartbeat").toString();
            String city = model.get("city") == null ? "" : model.get("city").toString();
            String upTime = dateUtil.get24HCurrentTime_Calendar("ymdhms");
            String date = dateUtil.get24HCurrentTime_Calendar("ymd");
            Resultgroup resultgroup = new Resultgroup();
            resultgroup.setUpTime(upTime);
            resultgroup.setDetectionId(detectionID);
            resultgroup.setUpUser(username);
            resultgroup.setPatientId(patientID);
            resultgroup.setCity(city);
            resultgroup.setDate(date);
            resultgroup.setIp(ipAddress);
            uploadMapper.insertResultGroup(resultgroup);
            long resultgroupId = resultgroup.getId();
            List imgNameList = (List) model.get("imageList");
            for (Object imgName : imgNameList) {
                String times = UploadUtils.times;
                String imgPath = IMAGE_PATH + File.separator + session.getAttribute("username") + File.separator + times + File.separator + imgName;
                /*File fileDirPath = UploadUtils.getFileDirPath(imgPath);
                String absolutePath = fileDirPath.getAbsolutePath();*/
                list.add(imgPath);
                Resultlog resultlog = new Resultlog();
                resultlog.setUpUser(username);
                resultlog.setUpTime(upTime);
                resultlog.setDetectionId(detectionID);
                resultlog.setPatientId(patientID);
                resultlog.setGroupId(resultgroupId);
                resultlog.setImgPath(imgPath);
                uploadMapper.insertResultLog(resultlog);
            }
            String listPath = String.join(",", list);
            JSONObject jsonObject = JSONUtil.createObj();
            jsonObject.put("arg01", resultgroupId);
            jsonObject.put("patient_id", patientID);
            jsonObject.put("detect_id", detectionID);
            jsonObject.put("list", list);
            jsonObject.put("username", username);
            jsonObject.put("heartbeat", heartbeat);
            String udpResult = AlgoUdp.getUdpAlgo(jsonObject);
            //String pythonRes = excutePython.getPythonRes(jsonObject);
            object = JSONUtil.parseObj(udpResult);
        }
        return object;
    }

    @Override
    public void updateResult(Map model,HttpSession session) {
        String upTime = dateUtil.get24HCurrentTime_Calendar("ymdhms");
        String upName = session.getAttribute("username").toString();
        if (!model.isEmpty()) {
            String resultGroupId = model.get("id").toString();
            String patientID = model.get("patientID").toString();
            String detectionID = model.get("detectionID").toString();
            String result = model.get("result").toString();
            String last = model.get("last").toString();
            String errorInfo = model.get("error").toString();
            String isResult = model.get("isResult").toString();
            if (errorInfo == "") {
                //update resultgroup
                uploadMapper.updateResultGroup(isResult,result,last,resultGroupId);
                //update resultlog
                uploadMapper.updateLog(isResult,result,last,resultGroupId);
            }else {
                uploadMapper.updateResultGroup1(isResult,result,last,resultGroupId,errorInfo);
                uploadMapper.updateLog1(isResult,resultGroupId);
                //往resulterror里面插数据
                Resulterror resulterror = new Resulterror();
                resulterror.setUpUser(upName);
                resulterror.setUpTime(upTime);
                resulterror.setPatientId(patientID);
                resulterror.setDetectionId(detectionID);
                resulterror.setGroupId(Long.parseLong(resultGroupId));
                resulterror.setErrInfo(errorInfo);
                uploadMapper.insertResultError(resulterror);
            }
        }
    }

    @Override
    public void updateResGroup(String id, String text) {
        uploadMapper.updateResGroup(id,text);
    }

    @Override
    public List getDoctoryText(String id) {
        List list = uploadMapper.selectDoctoryText(id);
        return list;
    }

    @Override
    public List getAllResGroup(String idArr) {
        List list = uploadMapper.getAllResGroup(idArr);
        return list;
    }

    @Override
    public List downloadGetAllResGroup(HttpSession session) {
        String upName = session.getAttribute("username").toString();
        List list = uploadMapper.downloadGetAllResGroup(upName);
        return list;
    }

    @Override
    public List getResultGroupData(HttpSession session,String id,String field,String order,String username) {

        String upName = "";
        if ("".equals(order)) {
            order = null;
        }

        if (!StrUtil.isEmpty(username)) {
            upName = username;
        } else {
            upName = session.getAttribute("username").toString();
        }

        List<Resultgroup> resultGroupData = uploadMapper.getResultGroupData(upName, id, field, order);
        return resultGroupData;
    }

    //未用到
    @Override
    public List selectRes(String text) {
        List list = uploadMapper.selectRes(text);
        return list;
    }

    @Override
    public String[] getResultGro(HttpSession session,String id) {
        String upName = session.getAttribute("username").toString();
        List<Resultlog> resultlog = uploadMapper.getResultGro(id);
        String[] arr = new String[resultlog.size()];
        for (int i = 0; i < resultlog.size(); i++) {
            Resultlog resultlog1 = resultlog.get(i);
            String imgPath = resultlog1.getImgPath();
            String substring = imgPath.substring(imgPath.lastIndexOf(File.separator)).substring(1);
//            String[] split = imgPath.split("/");
//            arr[i] = split[5];
            arr[i] = substring;
        }
        return arr;
    }

    @Override
    public Map<String, Object> getPath(HttpSession session, String id) {
        String upName = session.getAttribute("username").toString();
        List<Resultlog> resultlog = uploadMapper.getResultGro(id);
        String[] arr = new String[resultlog.size()];
        Map<String,Object> map = new HashMap<String, Object>();
        for (int i = 0; i < resultlog.size(); i++) {
            Resultlog resultlog1 = resultlog.get(i);
            String imgPath = resultlog1.getImgPath();
            String substring = imgPath.substring(0, imgPath.lastIndexOf(File.separator));
            String str = substring.substring(substring.lastIndexOf(File.separator)).substring(1);
//            String[] split = imgPath.split("/");
//            map.put("path",split[4]);
            map.put("path",str);
//            arr[i] = split[4];
        }
        return map;
    }

    @Override
    public int checkPatient(HttpSession session) {
        String upName = session.getAttribute("username").toString();
        int i = uploadMapper.checkPatient(upName);
        return i;
    }

    @Override
    public int checkIC(String ic) {
        int i = uploadMapper.checkIC(ic);
        if (i == 1) {
            uploadMapper.updateIC(ic);
            return i;
        }else {
            return i;
        }
    }

    @Override
    public int saveTempNumber(String number,String tempNumber) {
        int flag = 0;
        List<Iccard> iccards = uploadMapper.selectICByNumber(number);
        if (iccards.size() > 0) {
            uploadMapper.saveTempNumber(number,tempNumber);
            flag = 1;
        }
        return flag;
    }

    @Override
    public List<Iccard> selectCount(String ic) {
        List<Iccard> iccards = uploadMapper.selectCount(ic);
        return iccards;
    }

    @Override
    public List getReGroup(String id) {
        List<Resultlog> resultlog = uploadMapper.getResultGro(id);
        return resultlog;
    }

    @Override
    public List downloadGetReGroup(String id) {
        List<Resultlog> resultlog = uploadMapper.downloadGetReGroup(id);
        return resultlog;
    }

    @Override
    public List getGroup(String id) {
        List<Resultgroup> resultGroupData = uploadMapper.getGroup(id);
        return resultGroupData;
    }

    @Override
    public List getActivityCode(String id) {
        List<LoginCodeDTO> activityCode = uploadMapper.getActivityCode(id);
        return activityCode;
    }

    @Override
    public List getAllResGroupLog(String idArr) {
        List<Resultlog> resultlog = uploadMapper.getAllResGroupLog(idArr);
        return resultlog;
    }

    @Override
    public List downloadGetAllResGroupLog(HttpSession session) {
        String upName = session.getAttribute("username").toString();
        List<Resultlog> resultlog = uploadMapper.downloadGetAllResGroupLog(upName);
        return resultlog;
    }

    @Override
    public List getAllResGroupLogByUserId(String idArr) {
        List<Resultlog> resultlog = uploadMapper.getAllResGroupLogByUserId(idArr);
        return resultlog;
    }

    @Override
    public List getAllResGroupByUserId(String idArr) {
        List list = uploadMapper.getAllResGroupByUserId(idArr);
        return list;
    }

    @Override
    public void delResultGroup(String id) {
        uploadMapper.delResultGroup(id);
        uploadMapper.delResultLog(id);
    }
}
