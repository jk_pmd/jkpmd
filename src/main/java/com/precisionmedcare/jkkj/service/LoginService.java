package com.precisionmedcare.jkkj.service;

import com.precisionmedcare.jkkj.domain.Loginuser;
import com.precisionmedcare.jkkj.utils.Result;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;

public interface LoginService {
    List login(String username, String password);

    List<Loginuser> ClientLogin(String username, String password);

    void insertLoginLog(String username, HttpSession session, Result result, String loginType, String userid);

    void insertLogin(String username, String password, String group, String remark, String endTime,String code, String type);

    List checkPwd(String username, String oldPwd ,String type);

    void insertPwd(String id,String newPwd);

    void insertLoginCode();

    List checkCode(String code);

    List checkUsername(String name);

    void updateActiveCode(String name, String newcode);

    List checkNewCode(String newCode);

    List<HashMap> loginManage(String name, String pwd);

    List getUsers(String id);

    List selectByUser(String user);

    void deleteUser(String userId);

    int selectIC(String number);

    void saveIC(String number);

}
