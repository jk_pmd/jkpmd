package com.precisionmedcare.jkkj.service;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

public interface UploadSliceService {
    Object insertBLKResultGroup(Map model, String ipAddress, HttpSession session, String IMAGE_PATH, String timeStamp);
}
