package com.precisionmedcare.jkkj.config;

import org.springframework.core.annotation.Order;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Order(1)
@WebFilter(filterName = "sessionFilter",urlPatterns = {"/*"})
public class SessionFilter implements Filter {

    //标示符：表示当前用户未登录(可根据自己项目需要改为json样式)
    String NO_LOGIN = "ISAJAX";

    //不需要登录就可以访问的路径(比如:注册登录等)
    String[] includeUrls = new String[]{
            "/login/loginManage",
            "/login/insertLoginLog",
            "/login/getLoginUserData",
            "/login/checkNewCode",
            "/login/updateActiveCode",
            "/login/downloadChrome",
            "/upload/image",
            "/test",
            "/Bootstrap4.3.1/images/favicon.ico",
            "/Bootstrap4.3.1/js/date.js",
            "/page/lib/jquery.cookie.js",
            "/Bootstrap4.3.1/js/md5.js",
            "/Bootstrap4.3.1/js/bootstrap.min.js",
            "/Bootstrap4.3.1/js/jquery-2.1.0.min.js",
            "/Bootstrap4.3.1/js/common.js",
            "/Bootstrap4.3.1/js/bootstrap.min.js.map",
            "/page/lib/bootstrap/css/bootstrap2.css",
            "/page/stylesheets/theme.css",
            "/page/images/logo2.png",
            "/page/lib/font-awesome/css/font-awesome.css",
            "/Bootstrap4.3.1/js/date.format.js",
            "/Bootstrap4.3.1/js/error.js",
            "/favicon.ico",
            "/layer-v3.1.1/layer/layer.js",
            "/layer-v3.1.1/layer/theme/default/layer.css",
            "/layer-v3.1.1/layer/theme/default/icon.png",
            "/layer-v3.1.1/layer/theme/default/loading-0.gif",
            "/layer-v3.1.1/layer/theme/default/loading-1.gif",
            "/layer-v3.1.1/layer/theme/default/loading-2.gif",
            "/layer-v3.1.1/layer/theme/default/icon-ext.png",

            "/index.html",
            "/indexEnglish.html",
            "/",
            "/home/home.html",
            "/home/homeEnglish.html",

            "/error.html",
            "/regist/regist.html",
            "/regist/registEnglish.html"
    };


    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String userAgent = request.getHeader("user-agent").toLowerCase();;
        HttpSession session = request.getSession(false);
        String uri = request.getRequestURI();
//        PrintWriter writer = null;
        System.out.println("filter url:"+uri);
        //是否需要过滤
        boolean needFilter = isNeedFilter(uri);
        if (!needFilter) { //不需要过滤直接传给下一个过滤器
            filterChain.doFilter(servletRequest, servletResponse);
        } else { //需要过滤器
            // session中包含user对象,则是登录状态
            if(session!=null&&session.getAttribute("username") != null){
                // System.out.println("user:"+session.getAttribute("user"));
                filterChain.doFilter(request, response);
            }else{
                if (userAgent.indexOf("android") != -1 || userAgent.indexOf("iphone") != -1 || userAgent.indexOf("ipad") != -1 || userAgent.indexOf("ipod") != -1 || userAgent.indexOf("postman") != -1) {
                    filterChain.doFilter(request, response);
                }else {
                    String requestType = request.getHeader("X-Requested-With");
                    //判断是否是ajax请求
                    if(requestType!=null && "XMLHttpRequest".equals(requestType)){
                        // ajax请求
                        response.setHeader("SESSIONSTATUS", "TIMEOUT");
                        response.setHeader("CONTEXTPATH", "../index.html");
                        response.setStatus(HttpServletResponse.SC_FORBIDDEN);//403 禁止
//                    response.getWriter().write(this.NO_LOGIN);
                        //处理AJAX请求，设置响应头信息
                        /*   response.setHeader("REDIRECT", "REDIRECT");
                         *//*需要跳转页面的URL*//*
                    response.setHeader("CONTEXTPATH", request.getContextPath()+"/index.html");*/
                    }else{
                        //重定向到登录页(需要在static文件夹下建立此html文件)
                        response.sendRedirect(request.getContextPath() + "/index.html");
                    }
                    return;
                }
            }
        }
    }

/*
     * @Author: xxxxx
     * @Description: 是否需要过滤
     * @Date: 2018-03-12 13:20:54
     * @param uri
 */



    public boolean isNeedFilter(String uri) {

        for (String includeUrl : includeUrls) {
            if(includeUrl.equals(uri)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }
}
