package com.precisionmedcare.jkkj.domain;


public class Iccard {

  private long id;
  private String number;
  private long count;

  public String getTempnumber() {
    return tempnumber;
  }

  public void setTempnumber(String tempnumber) {
    this.tempnumber = tempnumber;
  }

  private String tempnumber;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }


  public long getCount() {
    return count;
  }

  public void setCount(long count) {
    this.count = count;
  }

}
