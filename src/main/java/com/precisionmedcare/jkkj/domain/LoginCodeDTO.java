package com.precisionmedcare.jkkj.domain;

public class LoginCodeDTO {
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getActivecode() {
        return activecode;
    }

    public void setActivecode(String activecode) {
        this.activecode = activecode;
    }

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public long getUserid() {
        return userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    private long id;
    private String activecode;
    private long status;
    private long userid;
    private String day;

}
