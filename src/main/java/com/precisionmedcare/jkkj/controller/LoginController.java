package com.precisionmedcare.jkkj.controller;


import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.precisionmedcare.jkkj.domain.Loginuser;
import com.precisionmedcare.jkkj.service.LoginService;
import com.precisionmedcare.jkkj.utils.Result;
import com.precisionmedcare.jkkj.utils.ZipUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;

@RestController
@RequestMapping("/login")
public class LoginController {
    @Autowired
    private LoginService loginService;

    @RequestMapping("getLoginUserData")
    public Result getLoginUserDta(String username, String password) {
        Result result = new Result();
        List list = null;
        try {
            list = loginService.login(username, password);
            if(list.isEmpty()){
                result.setSuccess(false);
            }else {
                result.setSuccess(true);
                result.setContent(list);
            }
        } catch (Exception e) {
            e.printStackTrace();
            result.setSuccess(false);
        }
        return result;
    }
    @RequestMapping("insertLoginLog")
    public Result insertLoginLog(String username, HttpSession session, String loginType, String userid) {
        Result result = new Result();
        try {
            loginService.insertLoginLog(username, session, result, loginType, userid);
            result.setSuccess(true);
        } catch (Exception e) {
            result.setSuccess(false);
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping("insertLogin")
    public Result insertLogin(String username, String password, String group, String remark, String endTime, String code, String type) {
        Result result = new Result();
        try {
            loginService.insertLogin(username, password, group, remark, endTime, code, type);
            result.setSuccess(true);
            result.setContent("注册成功");
        } catch (Exception e) {
            e.printStackTrace();
            result.setSuccess(false);
            result.setContent("注册失败，失败原因" + e.getMessage());
        }
        return result;
    }

    @RequestMapping("getUsername")
    public Result getUsername(HttpSession session) {
        Result result = new Result();
        Object username = session.getAttribute("username");
        result.setContent(username);
        return result;
    }

    @RequestMapping("checkPwd")
    public List checkPwd(String username, String oldPwd,String type ) {
        Result result = new Result();
        List list = null;
        try {
            list = loginService.checkPwd(username, oldPwd ,type);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
    @RequestMapping("insertPwd")
    public Result insertPwd(String id,String newPwd) {
        Result result = new Result();
        try {
            loginService.insertPwd(id,newPwd);
            result.setSuccess(true);
            result.setContent("密码修改成功");
        } catch (Exception e) {
            e.printStackTrace();
            result.setSuccess(false);
            result.setContent("密码修改失败，失败原因" + e.getMessage());
        }
        return result;
    }

    @RequestMapping("insertLoginCode")
    public Result insertLoginCode() {
        Result result = new Result();
        try {
            loginService.insertLoginCode();
            result.setSuccess(true);
        } catch (Exception e) {
            result.setSuccess(false);
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping("checkCode")
    public List checkCode(String code) {
        Result result = new Result();
        List list = null;
        try {
            list = loginService.checkCode(code);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @RequestMapping("checkUsername")
    public List checkUsername(String name) {
        Result result = new Result();
        List list = null;
        try {
            list = loginService.checkUsername(name);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @RequestMapping("updateActiveCode")
    public Result updateActiveCode(String name, String newcode) {
        Result result = new Result();
        try {
            loginService.updateActiveCode(name,newcode);
            result.setSuccess(true);
        } catch (Exception e) {
            e.printStackTrace();
            result.setSuccess(false);
        }
        return result;
    }

    @RequestMapping("checkNewCode")
    public Result checkNewCode(String newCode) {
        Result result = null;
        List list = null;
        try {
            list = loginService.checkNewCode(newCode);
            if(list.isEmpty()){
                result = new Result(false, null);
            }else {
                result = new Result(true,list);
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = new Result(false, null);
        }
        return result;
    }

    @RequestMapping("loginManage")
    public Result loginManage(String name,String password){
        Result result = new Result();
        List<HashMap> hashMaps = null;
        try {
            hashMaps = loginService.loginManage(name, password);
            result.setSuccess(true);
            result.setContent(hashMaps);
        } catch (Exception e) {
            e.printStackTrace();
            result.setSuccess(false);
            result.setContent(e.getMessage());
        }
        return result;
    }

    @RequestMapping("getUsers")
    public JSONObject getUsers(@RequestParam String id,@RequestParam int page ,@RequestParam int limit){
        PageHelper.startPage(page, limit);
        PageInfo<Loginuser> resultgroupPageInfo = new PageInfo<Loginuser>(loginService.getUsers(id));
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code",0);
        jsonObject.put("msg","");
        jsonObject.put("count",resultgroupPageInfo.getTotal());
        jsonObject.put("data",resultgroupPageInfo.getList());
        return jsonObject;
    }

    @RequestMapping("selectByUser")
    public List selectByUser(String user){
        List list = null;
        try {
            list = loginService.selectByUser(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @RequestMapping("deleteUser")
    public Result deleteUser(String userId){
        Result result = null;
        try {
           loginService.deleteUser(userId);
            result = new Result(true,"删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            result = new Result(true, "删除失败，失败原因：" + e.getMessage());
        }
        return result;
    }
    @RequestMapping("downloadChrome")
    public void downloadChrome(HttpServletRequest request, HttpServletResponse response) throws Exception{
   /*     File file = ResourceUtils.getFile("classpath:static/Chrome");
        if(file.exists()){
            File[] files = file.listFiles();
            if(files != null){
                for(File childFile:files){
                    //设置文件MIME类型
                    response.setContentType("application/octet-stream");
                    //设置Content-Disposition
                    response.setHeader("Content-Disposition", "attachment;filename=" + new String(childFile.getName().replaceAll(" ", "").getBytes("utf-8"), "iso8859-1"));
                    ServletOutputStream outputStream = response.getOutputStream();
                    InputStream inputStream = new FileInputStream(new File(String.valueOf(childFile)));
                    byte[] buffer = new byte[1024*1024];
                    int i = -1;
                    while ((i = inputStream.read(buffer)) != -1) {
                        outputStream.write(buffer, 0, i);
                    }
                    inputStream.close();
                    outputStream.flush();
                    outputStream.close();
                }
            }
        }*/
        File file = ResourceUtils.getFile("classpath:static/Chrome");
        String zipName = "chromeAndPlugs.zip";
        response.setContentType("APPLICATION/OCTET-STREAM");
        response.setHeader("Content-Disposition", "attachment; filename=" + zipName);
        ZipOutputStream out = new ZipOutputStream(response.getOutputStream());
        try {
            ZipUtils.doCompress(file, out);
            response.flushBuffer();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.close();
        }
    }
    @RequestMapping("downloadInstructions")
    public void downloadInstructions(HttpServletRequest request, HttpServletResponse response) throws Exception{
        File file = ResourceUtils.getFile("classpath:static/instructions");
        if(file.exists()){
            File[] files = file.listFiles();
            if(files != null){
                for(File childFile:files){
                    //设置文件MIME类型
                    response.setContentType("application/octet-stream");
                    //设置Content-Disposition
                    response.setHeader("Content-Disposition", "attachment;filename=" + new String(childFile.getName().replaceAll(" ", "").getBytes("utf-8"), "iso8859-1"));
                    ServletOutputStream outputStream = response.getOutputStream();
                    InputStream inputStream = new FileInputStream(new File(String.valueOf(childFile)));
                    byte[] buffer = new byte[1024*1024];
                    int i = -1;
                    while ((i = inputStream.read(buffer)) != -1) {
                        outputStream.write(buffer, 0, i);
                    }
                    inputStream.close();
                    outputStream.flush();
                    outputStream.close();
                }
            }
        }
    }
    @RequestMapping("saveIC/{number}")
    public Result saveIC(@PathVariable(value = "number") String number){
        Result result = new Result();
        int count = loginService.selectIC(number);
        if (count == 0) {
            try {
                loginService.saveIC(number);
                result.setSuccess(true);
            } catch (Exception e) {
                e.printStackTrace();
                result.setContent(e.getMessage());
            }
        }else {
            result.setSuccess(false);
        }

        return result;
    }
}
