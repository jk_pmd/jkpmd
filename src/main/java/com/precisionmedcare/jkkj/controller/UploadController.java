package com.precisionmedcare.jkkj.controller;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.precisionmedcare.jkkj.domain.Iccard;
import com.precisionmedcare.jkkj.domain.LoginCodeDTO;
import com.precisionmedcare.jkkj.domain.Resultgroup;
import com.precisionmedcare.jkkj.domain.Resultlog;
import com.precisionmedcare.jkkj.service.UploadService;
import com.precisionmedcare.jkkj.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.*;
import java.util.zip.ZipOutputStream;

@RestController
@RequestMapping("/upload")
public class UploadController {
    @Autowired
    private UploadService uploadService;
    @Value("${upload.path}")
    private String uploadPath;
    private final static String IMAGE_PATH = "static/upload/image/";

    @RequestMapping(value = "image", method = RequestMethod.POST)
    public Result upload(HttpServletRequest request, HttpSession session,String uuid) {
        Result msg = new Result();
        MultipartHttpServletRequest Murequest = (MultipartHttpServletRequest)request;
        Map<String, MultipartFile> files = Murequest.getFileMap();//得到文件map对象
        for(MultipartFile file :files.values()){

            if (file.isEmpty()) {
                // 设置错误状态码
                msg.setSuccess(false);
                msg.setContent("上传失败，请选择文件");
                return msg;
            }
            // 拿到文件名
            String oldfilename = file.getOriginalFilename();

            String filename = oldfilename.replace(" ", "_");

            // 存放上传图片的文件夹
            File fileDir = UploadUtils.getImgDirFile(uploadPath,session,uuid);
            // 输出文件夹绝对路径  -- 这里的绝对路径是相当于当前项目的路径而不是“容器”路径
//            System.out.println(fileDir.getAbsolutePath());
            try {
                // 构建真实的文件路径
                File newFile = new File(fileDir.getAbsolutePath() + File.separator + filename);
//                System.out.println(newFile.getAbsolutePath());

                // 上传图片到 -》 “绝对路径”
                file.transferTo(newFile);
                msg.setSuccess(true);
                msg.setContent(filename);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return msg;
    }

    @RequestMapping(value = "insertResultGroup", method = RequestMethod.POST)
    public Result insertResultGroup(HttpServletRequest request, @RequestBody Map model,HttpSession session) {
        Result result = new Result();
        String ipAddress = GetAddressIP.getClientIpAddress(request);
        try {
            Object pythonResult = uploadService.insertResultGroup(model, ipAddress, session, uploadPath);
            /*
            *   执行成功执行udp编程调用python接口
            * */
            result.setSuccess(true);
            result.setContent(pythonResult);
        } catch (Exception e) {
            result.setSuccess(false);
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(value = "updateResult", method = RequestMethod.POST)
    public Result updateResult(@RequestBody Map model,HttpSession session) {
        Result result = new Result();
        try {
            uploadService.updateResult(model,session);
            result.setSuccess(true);
        } catch (Exception e) {
            result.setSuccess(false);
            e.printStackTrace();
        }
        return result;
    }
    @RequestMapping(value = "resultSave", method = RequestMethod.POST)
    public Result resultSave(String id,String text) {
        Result result = new Result();
        try {
            uploadService.updateResGroup(id,text);
            result.setSuccess(true);
        } catch (Exception e) {
            result.setSuccess(false);
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(value = "getText", method = RequestMethod.POST)
    public List getText(String id) {
        List doctoryText = null;
        try {
            doctoryText = uploadService.getDoctoryText(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return doctoryText;
    }

/*    @RequestMapping(value = "getResultGroupData", method = RequestMethod.POST)
    public List getResultGroupData(HttpSession session) {
        List doctoryText = null;
        try {
            doctoryText = uploadService.getResultGroupData(session);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return doctoryText;
    }*/

    @RequestMapping(value = "getResultGroupData", method = RequestMethod.GET)
    public JSONObject getResultGroupData(HttpSession session,@RequestParam int page ,@RequestParam int limit,@RequestParam String id, @RequestParam(required = false) String field,@RequestParam(required = false) String order,@RequestParam(required = false) String username) {
        PageHelper.startPage(page, limit);
        PageInfo<Resultgroup> resultgroupPageInfo = new PageInfo<Resultgroup>(uploadService.getResultGroupData(session, id,field,order,username));
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code",0);
        jsonObject.put("msg","");
        jsonObject.put("count",resultgroupPageInfo.getTotal());
        jsonObject.put("data",resultgroupPageInfo.getList());
        return jsonObject;
    }
    @RequestMapping(value = "selectRes", method = RequestMethod.POST)
    public List selectRes(String text) {
        List doctoryText = null;
        try {
            doctoryText = uploadService.selectRes(text);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return doctoryText;
    }
    @RequestMapping(value = "getResultGro", method = RequestMethod.GET)
    public String[] getResultGro(HttpSession session,@RequestParam  String id) {
        String[] doctoryText = null;
        try {
            doctoryText = uploadService.getResultGro(session,id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return doctoryText;
    }

    @RequestMapping(value = "getPath", method = RequestMethod.GET)
    public Map<String, Object> getPath(HttpSession session,@RequestParam  String id) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map = uploadService.getPath(session,id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    @RequestMapping(value = "getGroup", method = RequestMethod.GET)
    public JSONObject getGroup(@RequestParam int page ,@RequestParam int limit,@RequestParam String id) {
        PageHelper.startPage(page, limit);
        PageInfo<Resultgroup> objectPageInfo = new PageInfo<Resultgroup>(uploadService.getGroup(id));
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code",0);
        jsonObject.put("msg","");
        jsonObject.put("count",objectPageInfo.getTotal());
        jsonObject.put("data",objectPageInfo.getList());
        return jsonObject;
    }

    @RequestMapping(value = "getActivityCode", method = RequestMethod.GET)
    public JSONObject getActivityCode(@RequestParam int page ,@RequestParam int limit,@RequestParam String id) {
        PageHelper.startPage(page, limit);
        PageInfo<LoginCodeDTO> logincodePageInfo = new PageInfo<LoginCodeDTO>(uploadService.getActivityCode(id));
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code",0);
        jsonObject.put("msg","");
        jsonObject.put("count",logincodePageInfo.getTotal());
        jsonObject.put("data",logincodePageInfo.getList());
        return jsonObject;
    }

    @RequestMapping(value = "checkPatient", method = RequestMethod.POST)
    public Result checkPatient(HttpSession session) {
        Result result = new Result();
        try {
            int i = uploadService.checkPatient(session);
            result.setSuccess(true);
            result.setContent(i);
        } catch (Exception e) {
            e.printStackTrace();
            result.setSuccess(true);
        }
        return result;
    }

    @RequestMapping(value = "checkIC/{ic}", method = RequestMethod.POST)
    public Result checkIC(@PathVariable(value = "ic") String ic) {
        Result result = new Result();
        int icCount = uploadService.checkIC(ic);
        if (icCount == 1) {
            result.setSuccess(true);
        }else {
            result.setSuccess(false);
        }
        return result;
    }

    @RequestMapping(value = "saveTempNumber", method = RequestMethod.POST)
    public Result saveTempNumber(String number,String tempNumber) {
        Result result = new Result();
        try {
            int i = uploadService.saveTempNumber(number, tempNumber);
            if (i == 0) {
                result.setSuccess(false);
                result.setContent(0);
            }else {
                result.setSuccess(true);
                result.setContent(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
            result.setSuccess(false);
            result.setContent("");
        }
        return result;
    }

    @RequestMapping(value = "selectCount/{icKA}", method = RequestMethod.GET)
    public Result selectCount(@PathVariable(value = "icKA") String icKA) {
        Result result = new Result();
        List<Iccard> iccards = uploadService.selectCount(icKA);
        if (iccards.size() != 0) {
            if (iccards.get(0).getCount() > 0) {
                result.setSuccess(true);
                result.setContent(iccards.get(0).getCount());
            } else {
                result.setSuccess(false);
                result.setContent("该精康检测卡次数已用完，请联系管管理员/The JK card has been used up, please contact the administrator");
            }
        } else {
            result.setSuccess(false);
            result.setContent("该精康检测卡尚未注册/The JK card has not been registered");
        }
        return result;
    }

    /**
     * @return
     * @author qlh
     * @creed: Talk is cheap,show me the code
     * @date
     */
    @RequestMapping("downloadFile")
    public void downloadFile(@RequestParam String id, @RequestParam String dirPath ,HttpServletRequest request, HttpServletResponse response,HttpSession session) throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
//        List<Resultlog> resultGroupLog = uploadService.getReGroup(id);
        List<Resultlog> resultGroupLog = uploadService.downloadGetReGroup(id);
        List resultGroup = uploadService.getDoctoryText(id);
        Resultgroup resultgroup = (Resultgroup) resultGroup.get(0);
        String msg = "上传记录：" + resultgroup.getId() + "上传医生：" + resultgroup.getUpUser() + "上传时间：" + resultgroup.getUpTime()
                + "病人编号：" + resultgroup.getPatientId() + "检测编号：" + resultgroup.getDetectionId() + "检测结果：" + resultgroup.getReResult()
                + "错误信息：" + resultgroup.getErrInfo() + "备注：" + resultgroup.getReText();
        // 构建上传文件的存放 "文件夹" 路径
        String fileDirPath = new String(uploadPath + File.separator + session.getAttribute("username") + File.separator + dirPath + File.separator + "recode.txt");
        File fileDir = new File(fileDirPath);
        /*if(!fileDir.exists()){
            // 递归生成文件夹
            fileDir.mkdirs();
        }*/
        try {
            fileDir.createNewFile();
            BufferedWriter out = new BufferedWriter(new FileWriter(fileDir));
            out.write(msg); // \r\n即为换行
            out.flush(); // 把缓存区内容压入文件
            out.close(); // 最后记得关闭文件

        } catch (IOException e) {
            e.printStackTrace();
        }
        String zipName = "SingleRecode.zip";
        response.setContentType("APPLICATION/OCTET-STREAM");
        response.setHeader("Content-Disposition", "attachment; filename=" + zipName);
        ZipOutputStream out = new ZipOutputStream(response.getOutputStream());
        try {
//            for (Iterator it = resultGroupLog.iterator(); it.hasNext(); ) {
//                Resultlog resultlog = (Resultlog) it.next();
                String imgPath = resultGroupLog.get(0).getImgPath();
                String path = imgPath.substring(0, imgPath.lastIndexOf(File.separator));
                ZipUtils.doCompress(path, out);
                response.flushBuffer();
//            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.close();
        }
/*        String zipName = UUID.randomUUID().toString().replaceAll("-", "").toUpperCase()+".zip";
//        String outFilePath = request.getSession().getServletContext().getRealPath("/")+"upload";
        String outFilePath = request.getSession().getServletContext().getRealPath("/");
        File fileZip = new File(outFilePath + zipName);
        for (int i = 0; i < resultGroupLog.size(); i++) {
            Resultlog resultlog = resultGroupLog.get(i);
            String imgPath = resultlog.getImgPath();
            String path = imgPath.substring(0, imgPath.lastIndexOf("/"));
            try {
                List<File> fileList = FileUtil.getFiles("src/main/resources/" +path);
                FileOutputStream outStream = new FileOutputStream(fileZip);
                ZipOutputStream toClient = new ZipOutputStream(outStream);
                IOtools.zipFile(fileList, toClient);
                toClient.close();
                outStream.close();
                IOtools.downloadFile(fileZip, response, true);
//                    return null;
                //单个文件下载
//                 for (int i = 0; i < fileList.size(); i++) {
//                 String curpath = fileList.get(i).getPath();//获取文件路径
//                 iconNameList.add(curpath.substring(curpath.lastIndexOf(File.separator) + 1));//将文件名加入数组
//
//                 String fileName = new String(filecomplaintpath.getBytes("UTF-8"),"iso8859-1");
//                 headers.setContentDispositionFormData("attachment", fileName);
//                 return new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(new File(filecomplaintpath)),
//                 headers, HttpStatus.OK);
//                 }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/
    }
    @RequestMapping("downloadAllFile")
    public void downloadAllFile(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
//        List<Resultlog> resultGroupLog = uploadService.getAllResGroupLog(idArr);
//        List resultGroup = uploadService.getAllResGroup(idArr);
        List<Resultlog> resultGroupLog = uploadService.downloadGetAllResGroupLog(session);
        List resultGroup = uploadService.downloadGetAllResGroup(session);
        Resultgroup resultgroup = null;
        String msg = "";
        for (int i = 0; i < resultGroup.size(); i++) {
            resultgroup = (Resultgroup) resultGroup.get(i);
            Map<String, Object> path = getPath(session, String.valueOf(resultgroup.getId()));
            String path1 = path.get("path").toString();
            msg += "上传记录：" + resultgroup.getId() + "上传医生：" + resultgroup.getUpUser() + "上传时间：" + resultgroup.getUpTime()
                    + "病人编号：" + resultgroup.getPatientId() + "检测编号：" + resultgroup.getDetectionId() + "检测结果：" + resultgroup.getReResult()
                    + "错误信息：" + resultgroup.getErrInfo() + "备注：" + resultgroup.getReText();
            // 构建上传文件的存放 "文件夹" 路径
            String fileDirPath = new String(uploadPath + File.separator + session.getAttribute("username") + File.separator + path1 + File.separator + "recode.txt");
            File fileDir = new File(fileDirPath);
            /*if (!fileDir.exists()) {
                // 递归生成文件夹
                fileDir.mkdirs();
            }*/
            try {
                fileDir.createNewFile();
                BufferedWriter out = new BufferedWriter(new FileWriter(fileDir));
                out.write(msg); // \r\n即为换行
                out.flush(); // 把缓存区内容压入文件
                out.close(); // 最后记得关闭文件

            } catch (IOException e) {
                e.printStackTrace();
            }
            msg = "";
        }
        String zipName = "MultipleRecode.zip";
        response.setContentType("APPLICATION/OCTET-STREAM");
        response.setHeader("Content-Disposition", "attachment; filename=" + zipName);
        ZipOutputStream out = new ZipOutputStream(response.getOutputStream());
        try {
//            for (Iterator it = resultGroupLog.iterator(); it.hasNext(); ) {
//                Resultlog resultlog = (Resultlog) it.next();
                String imgPath = resultGroupLog.get(0).getImgPath();
                String path = imgPath.substring(0, imgPath.lastIndexOf(File.separator));
                String DirPath = path.substring(0, path.lastIndexOf(File.separator));
                ZipUtils.doCompress(DirPath, out);
                response.flushBuffer();
//            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.close();
        }
    }

    @RequestMapping("downloadAllManageFile")
    public void downloadAllManageFile(@RequestParam String idArr,@RequestParam String username,HttpServletRequest request, HttpServletResponse response,HttpSession session) throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        List<Resultlog> resultGroupLog = uploadService.getAllResGroupLogByUserId(username);
        List resultGroup = uploadService.getAllResGroupByUserId(username);
        Resultgroup resultgroup = null;
        String msg = "";
        for (int i = 0; i < resultGroup.size(); i++) {
            resultgroup = (Resultgroup) resultGroup.get(i);
            Map<String, Object> path = getPath(session, String.valueOf(resultgroup.getId()));
            String path1 = path.get("path").toString();
            msg += "上传记录：" + resultgroup.getId() + "上传医生：" + resultgroup.getUpUser() + "上传时间：" + resultgroup.getUpTime()
                    + "病人编号：" + resultgroup.getPatientId() + "检测编号：" + resultgroup.getDetectionId() + "检测结果：" + resultgroup.getReResult()
                    + "错误信息：" + resultgroup.getErrInfo() + "备注：" + resultgroup.getReText();
            // 构建上传文件的存放 "文件夹" 路径
            String fileDirPath = new String(uploadPath + File.separator + username + File.separator + path1 + File.separator + "recode.txt");
            File fileDir = new File(fileDirPath);
            /*if (!fileDir.exists()) {
                // 递归生成文件夹
                fileDir.mkdirs();
            }*/
            try {
                fileDir.createNewFile();
                BufferedWriter out = new BufferedWriter(new FileWriter(fileDir));
                out.write(msg); // \r\n即为换行
                out.flush(); // 把缓存区内容压入文件
                out.close(); // 最后记得关闭文件

            } catch (IOException e) {
                e.printStackTrace();
            }
            msg = "";
        }
        String zipName = "MultipleRecode.zip";
        response.setContentType("APPLICATION/OCTET-STREAM");
        response.setHeader("Content-Disposition", "attachment; filename=" + zipName);
        ZipOutputStream out = new ZipOutputStream(response.getOutputStream());
        try {
//            for (Iterator it = resultGroupLog.iterator(); it.hasNext(); ) {
//                Resultlog resultlog = (Resultlog) it.next();
                String imgPath = resultGroupLog.get(0).getImgPath();
                String path = imgPath.substring(0, imgPath.lastIndexOf(File.separator));
                String DirPath = path.substring(0, path.lastIndexOf(File.separator));
                ZipUtils.doCompress(DirPath, out);
                response.flushBuffer();
//            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.close();
        }
    }

    /**
     * @return
     * @author qlh
     * @creed: Talk is cheap,show me the code
     * @date
     */
    @RequestMapping("downloadManageFile")
    public void downloadManageFile(@RequestParam String id, @RequestParam String dirPath , @RequestParam String username ,HttpServletRequest request, HttpServletResponse response,HttpSession session) throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        List<Resultlog> resultGroupLog = uploadService.downloadGetReGroup(id);
        List resultGroup = uploadService.getDoctoryText(id);
        Resultgroup resultgroup = (Resultgroup) resultGroup.get(0);
        String msg = "上传记录：" + resultgroup.getId() + "上传医生：" + resultgroup.getUpUser() + "上传时间：" + resultgroup.getUpTime()
                + "病人编号：" + resultgroup.getPatientId() + "检测编号：" + resultgroup.getDetectionId() + "检测结果：" + resultgroup.getReResult()
                + "错误信息：" + resultgroup.getErrInfo() + "备注：" + resultgroup.getReText();
        // 构建上传文件的存放 "文件夹" 路径
        String fileDirPath = new String(uploadPath + File.separator + username + File.separator + dirPath + File.separator + "recode.txt");
        File fileDir = new File(fileDirPath);
        /*if(!fileDir.exists()){
            // 递归生成文件夹
            fileDir.mkdirs();
        }*/
        try {
            fileDir.createNewFile();
            BufferedWriter out = new BufferedWriter(new FileWriter(fileDir));
            out.write(msg); // \r\n即为换行
            out.flush(); // 把缓存区内容压入文件
            out.close(); // 最后记得关闭文件

        } catch (IOException e) {
            e.printStackTrace();
        }
        String zipName = "SingleRecode.zip";
        response.setContentType("APPLICATION/OCTET-STREAM");
        response.setHeader("Content-Disposition", "attachment; filename=" + zipName);
        ZipOutputStream out = new ZipOutputStream(response.getOutputStream());
        try {
//            for (Iterator it = resultGroupLog.iterator(); it.hasNext(); ) {
//                Resultlog resultlog = (Resultlog) it.next();
                String imgPath = resultGroupLog.get(0).getImgPath();
                String path = imgPath.substring(0, imgPath.lastIndexOf(File.separator));
                ZipUtils.doCompress(path, out);
                response.flushBuffer();
//            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.close();
        }
    }

    @RequestMapping("delResultGroup/{id}")
    public Result delResultGroup(@PathVariable(value = "id") String id) {
        Result result = new Result();
        try{
            uploadService.delResultGroup(id);
            result.setSuccess(true);
        }catch (Exception e){
            result.setSuccess(false);
            result.setContent(e.getMessage());
        }
        return result;
    }
}
