package com.precisionmedcare.jkkj.controller;

import com.precisionmedcare.jkkj.service.UploadSliceService;
import com.precisionmedcare.jkkj.utils.GetAddressIP;
import com.precisionmedcare.jkkj.utils.Result;
import com.precisionmedcare.jkkj.utils.ResultAnother;
import com.precisionmedcare.jkkj.utils.UploadUtils;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping("/uploadSlice")
public class UploadSliceController {
    @Value("${upload.path}")
    private String uploadPath;

    public static String timeStamp;
    @Autowired
    private UploadSliceService uploadSliceService;

    @PostMapping("part")
    public ResultAnother<Object> bigFile(HttpServletRequest request, HttpServletResponse response, HttpSession session, String guid, Integer chunk, MultipartFile file, Integer chunks) {
        try {
//            String projectUrl = System.getProperty("user.dir").replaceAll("\\\\", "/");
            boolean isMultipart = ServletFileUpload.isMultipartContent(request);
            if (isMultipart) {
                if (chunk == null) chunk = 0;
                // 临时目录用来存放所有分片文件
//                String tempFileDir = projectUrl + "/upload/" + guid;
                String tempFileDir = uploadPath + File.separator + session.getAttribute("username") + File.separator + guid;

                File parentFileDir = new File(tempFileDir);
                if (!parentFileDir.exists()) {
                    parentFileDir.mkdirs();
                }
                // 分片处理时，前台会多次调用上传接口，每次都会上传文件的一部分到后台
                File tempPartFile = new File(parentFileDir, guid + "_" + chunk + ".part");
                FileUtils.copyInputStreamToFile(file.getInputStream(), tempPartFile);
            }
        } catch (Exception e) {
            return ResultAnother.failMessage(400, e.getMessage());
        }
        return ResultAnother.successMessage(200, "上传成功");
    }

    @RequestMapping("merge")
    public ResultAnother<Object> mergeFile(String guid, String fileName,HttpSession session) {
        // 得到 destTempFile 就是最终的文件
//        String projectUrl = System.getProperty("user.dir").replaceAll("\\\\", "/");
        try {
            String sname = fileName.substring(fileName.lastIndexOf("."));
            String replace = fileName.replace(" ", "_");

            //时间格式化格式
            Date currentTime = new Date();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
            //获取当前时间并作为时间戳
            timeStamp = simpleDateFormat.format(currentTime);
            //拼接新的文件名
//            String newName = timeStamp + sname;
            simpleDateFormat = new SimpleDateFormat("yyyyMM");
//            String path = projectUrl + "/upload/";
            String path = uploadPath + File.separator + session.getAttribute("username") + File.separator;
//            String tmp = simpleDateFormat.format(currentTime);
            File parentFileDir = new File(path + guid);
            if (parentFileDir.isDirectory()) {
                File destTempFile = new File(path + timeStamp, replace);
                if (!destTempFile.exists()) {
                    //先得到文件的上级目录，并创建上级目录，在创建文件
                    destTempFile.getParentFile().mkdir();
                    try {
                        destTempFile.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                for (int i = 0; i < parentFileDir.listFiles().length; i++) {
                    File partFile = new File(parentFileDir, guid + "_" + i + ".part");
                    FileOutputStream destTempfos = new FileOutputStream(destTempFile, true);
                    //遍历"所有分片文件"到"最终文件"中
                    FileUtils.copyFile(partFile, destTempfos);
                    destTempfos.close();
                }
                // 删除临时目录中的分片文件
                FileUtils.deleteDirectory(parentFileDir);
                return ResultAnother.successMessage(200, "合并成功");
            } else {
                return ResultAnother.failMessage(400, "没找到目录");
            }

        } catch (Exception e) {
            return ResultAnother.failMessage(400, e.getMessage());
        }

    }
    @RequestMapping(value = "insertBLKResultGroup", method = RequestMethod.POST)
    public Result insertBLKResultGroup(HttpServletRequest request, @RequestBody Map model, HttpSession session) {
        Result result = new Result();
        String ipAddress = GetAddressIP.getClientIpAddress(request);
        try {
            Object pythonResult = uploadSliceService.insertBLKResultGroup(model, ipAddress, session, uploadPath, timeStamp);
            /*
             *   执行成功执行udp编程调用python接口
             * */
            result.setSuccess(true);
            result.setContent(pythonResult);
        } catch (Exception e) {
            result.setSuccess(false);
            e.printStackTrace();
        }
        return result;
    }
}
