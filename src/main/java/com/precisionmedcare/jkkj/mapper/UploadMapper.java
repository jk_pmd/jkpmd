package com.precisionmedcare.jkkj.mapper;

import com.precisionmedcare.jkkj.domain.*;
import org.apache.ibatis.annotations.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Mapper
public interface UploadMapper {
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    @Insert("INSERT INTO resultgroup(upUser,patientId,detectionId,upTime,date,ip,city) VALUES(#{upUser}, #{patientId},#{detectionId},#{upTime}, #{date},#{ip},#{city})")
    void insertResultGroup(Resultgroup resultgroup);

    @Insert("INSERT INTO resultlog(upUser,patientId,detectionId,upTime,groupId,imgPath) VALUES(#{upUser}, #{patientId},#{detectionId},#{upTime},#{groupId},#{imgPath})")
    void insertResultLog(Resultlog resultlog);

    @Update("UPDATE resultgroup set isResult = #{isResult},reResult=#{result},reText=#{last} where id = #{resultGroupId} ")
    void updateResultGroup(@Param("isResult") String isResult, @Param("result") String result, @Param("last") String last, @Param("resultGroupId") String resultGroupId);

    @Update("UPDATE resultgroup set isResult = #{isResult},reResult=#{result},reText=#{last},errInfo =#{errorInfo},isErr=1 where id = #{resultGroupId} ")
    void updateResultGroup1(@Param("isResult") String isResult, @Param("result") String result, @Param("last") String last, @Param("resultGroupId") String resultGroupId, @Param("errorInfo") String errorInfo);

    @Update("UPDATE resultlog set reResult=#{result},reText=#{last},isRe =#{isResult} where groupId =#{resultGroupId} ")
    void updateLog(@Param("isResult") String isResult, @Param("result") String result, @Param("last") String last, @Param("resultGroupId") String resultGroupId);

    @Update("UPDATE resultlog set isRe =#{isResult} where groupId = #{resultGroupId} ")
    void updateLog1(@Param("isResult") String isResult,@Param("resultGroupId") String resultGroupId);

    @Insert("INSERT INTO resulterror(upUser,upTime,detectionId,patientId,groupId,errInfo) VALUES(#{upUser},#{upTime},#{detectionId},#{patientId},#{groupId},#{errInfo}) ")
    void insertResultError(Resulterror resulterror);

    @Update("UPDATE resultgroup SET doctorNotes=#{text} where id = #{id} ")
    void updateResGroup(@Param("id") String id,@Param("text") String text);

    @Select("select * from resultgroup where id = #{id} ")
    List<Resultgroup>  selectDoctoryText(@Param("id") String id);

    @Select("select * from resultgroup where id in (${idArr}) ")
    List<Resultgroup>  getAllResGroup(@Param("idArr") String idArr);

    @Select("select * from resultgroup where upUser = #{name} and isDel != 1 ")
    List<Resultgroup>  downloadGetAllResGroup(@Param("name") String upName);

    @Select("<script>" + "select * from resultgroup where upUser = #{upName} and isDel != 1 "
            + "<if test='text!=\"\"'> and (patientId = #{text} or detectionId = #{text}) </if>"
          /*  + "<when test='field == \"patientId\" and order != null'> order by patientId+0 ${order} </when>"
            + "<when test='field == \"detectionId\" and order != null'> order by detectionId+0 ${order} </when>"
            + "<when test='field == \"upTime\" and order != null'> order by upTime ${order} </when>"*/
            + "<when test='field != null and order != null'> order by convert(${field},signed) ${order} </when>"
            + "<when test='field == null and order == null'> order by upTime desc </when>"
            + "<when test='field != null and order == null'> order by upTime desc </when>"
            + "</script>")
    List<Resultgroup> getResultGroupData(@Param("upName") String upName, @Param("text") String id,@Param("field") String field, @Param("order") String order);

    @Select("select * from resultgroup where patientId = #{text}  or detectionId = #{text} order by upTime desc ")
    List<Resultgroup> selectRes(@Param("text") String text);

    @Select("select * from resultlog where groupId=#{id};")
    List<Resultlog> getResultGro(@Param("id") String id);

    @Select("select * from resultlog where groupId=#{id} limit 1;")
    List<Resultlog> downloadGetReGroup(@Param("id") String id);

    @Select("select * from resultlog where groupId in (${idArr});")
    List<Resultlog> getAllResGroupLog(@Param("idArr") String idArr);

    @Select("select * from resultlog where upUser = #{name} and isDel != 1 limit 1;")
    List<Resultlog> downloadGetAllResGroupLog(@Param("name") String upName);

    @Select("select MAX(patientId+0) maxPatientId from resultgroup where upUser =#{name} and isDel != 1")
    int checkPatient(@Param("name") String upName);

    @Select(" select count(*) from iccard where number = #{ic} or tempnumber = #{ic} and count > 0;")
    int checkIC(@Param("ic") String ic);

    @Select(" select * from iccard where number = #{ic} or tempnumber = #{ic}")
    List<Iccard> selectCount(@Param("ic") String ic);

    @Update("update iccard\n" +
            "set count = count - 1\n" +
            "where number = #{ic} " +
            "or tempnumber = #{ic}")
    void updateIC(@Param("ic") String ic);

    @Select("select * from iccard where number = #{number}")
    List<Iccard> selectICByNumber(@Param("number") String number);

    @Update("update iccard set tempnumber = #{tempNumber} where number = #{number}")
    void saveTempNumber(@Param("number") String number, @Param("tempNumber") String tempNumber);

    @Select("<script>" + "select * from resultgroup where 1=1 and isDel != 1  "
            + "<if test='text!=\"\"'> and (patientId = #{text} or detectionId = #{text} or upUser like \"%\"#{text}\"%\") </if>"
            + "order by upTime desc"
            + "</script>")
    List<Resultgroup>  getGroup(@Param("text")String id);

    @Select("<script>" + "select * ,datediff(left(logincode.activecode, 8), DATE_FORMAT(now(), '%Y%m%d')) day from logincode"
            + " where status = 1\n"
            + "  and datediff(left(logincode.activecode, 8), DATE_FORMAT(now(), '%Y%m%d')) > 0"
            + "  and userid is null"
            + "<if test='text!=\"\"'>  and activecode = #{text} </if>"
            + "</script>")
    List<LoginCodeDTO> getActivityCode(@Param("text")String id);

    @Select("select * from resultgroup where resultgroup.upUser = #{idArr} and resultgroup.isDel != 1")
    List<Resultgroup>  getAllResGroupByUserId(@Param("idArr") String idArr);

    @Select("select * from resultlog where resultlog.upUser = #{idArr} and resultlog.isDel != 1  limit 1;")
    List<Resultlog> getAllResGroupLogByUserId(@Param("idArr") String idArr);

    @Update("update resultgroup set isDel = 1 where id = #{id}")
    void delResultGroup(@Param("id") String id);

    @Update("update resultlog set isDel = 1 where groupId = #{id}")
    void delResultLog(@Param("id") String id);
}
