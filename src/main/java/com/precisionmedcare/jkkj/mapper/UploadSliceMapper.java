package com.precisionmedcare.jkkj.mapper;

import com.precisionmedcare.jkkj.domain.Resultgroup;
import com.precisionmedcare.jkkj.domain.Resultlog;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;

@Mapper
public interface UploadSliceMapper {
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    @Insert("INSERT INTO resultgroup(upUser,patientId,detectionId,upTime,date,ip,city) VALUES(#{upUser}, #{patientId},#{detectionId},#{upTime}, #{date},#{ip},#{city})")
    void insertBLKResultGroup(Resultgroup resultgroup);

    @Insert("INSERT INTO resultlog(upUser,patientId,detectionId,upTime,groupId,imgPath) VALUES(#{upUser}, #{patientId},#{detectionId},#{upTime},#{groupId},#{imgPath})")
    void insertBLKResultLog(Resultlog resultlog);


}
