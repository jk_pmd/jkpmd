package com.precisionmedcare.jkkj;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@MapperScan("com.precisionmedcare.jkkj.mapper")
@ServletComponentScan
public class JkkjApplication {

    public static void main(String[] args) {
        SpringApplication.run(JkkjApplication.class, args);
    }

}
