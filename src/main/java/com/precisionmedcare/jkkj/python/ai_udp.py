# -*- coding: utf-8 -*-
#import logging
from socketserver import BaseRequestHandler, TCPServer, UDPServer, ThreadingUDPServer
import time, random, json


#logging.basicConfig(format='%(asctime)s %(levelname)s:%(message)s', level=logging.INFO)

class UDPHandler(BaseRequestHandler):
    def handle(self):
        #print('Got connection from', self.client_address)
        start = time.time()
        msg, sock = self.request
        print(msg)
        msg = json.loads(msg)
        rst = {}
        num = int(random.uniform(1, 15)*10) / 10
        if num > -1:
        	ai_ret = num
        else:
        	ai_ret = "error, something wrong!!!"
        if isinstance(ai_ret, float):
            rst['result'] = int(ai_ret * 10) / 10.0
            rst['error'] = ''
        else:
            rst['result'] = ''
            rst['error'] = ai_ret
        rst['arg01'] = msg['arg01']
        rst['patient_id'] = msg['patient_id']
        rst['detect_id'] = msg['detect_id']
        rst['username'] = msg['username']
        rst['last'] = ''

        r = json.dumps(rst)
        sock.sendto(str.encode(r), self.client_address)


if __name__ == '__main__':
    print('starts:')
    serv = ThreadingUDPServer(('', 62000), UDPHandler)
    serv.serve_forever()
