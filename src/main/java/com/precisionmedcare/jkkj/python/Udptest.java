package com.precisionmedcare.jkkj.python;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.net.DatagramSocket;


public class Udptest {
    public static void main(String[] args) {
        String msg = "{\"username\": \"test\", \"arg01\": 519, \"heartbeat\": \"60\", \"patient_id\": \"123456\", \"detect_id\": \"123456\", \"list\": [\"/root/workspace/server/public/ultimage/test/2019-11-17/123456_123456/220154/519_0.jpg\", \"/root/workspace/server/public/ultimage/test/2019-11-17/123456_123456/220154/519_1.jpg\", \"/root/workspace/server/public/ultimage/test/2019-11-17/123456_123456/220154/519_2.jpg\", \"/root/workspace/server/public/ultimage/test/2019-11-17/123456_123456/220154/519_3.jpg\"]}";
        byte[] buf = msg.getBytes();
        try {
            InetAddress address = InetAddress.getLocalHost();
            int port = 62000;  //服务器的端口号
            //创建发送方的数据报信息
            DatagramPacket dataGramPacket = new DatagramPacket(buf, buf.length, address, port);

            DatagramSocket socket = new DatagramSocket();  //创建套接字
            socket.send(dataGramPacket);  //通过套接字发送数据

            //接收服务器反馈数据
            byte[] backbuf = new byte[1024];
            DatagramPacket backPacket = new DatagramPacket(backbuf, backbuf.length);
            socket.receive(backPacket);  //接收返回数据
            String backMsg = new String(backbuf, 0, backPacket.getLength());
            System.out.println("服务器返回的数据为:" + backMsg);

            socket.close();

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}