package com.precisionmedcare.jkkj.utils;

import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class MyTimerTask{
    public static void main(String[] args) {
        time1();
    }
    public static void time1() {
        // 定时器对象
        Timer timer = new Timer();
        // 定时器任务对象
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                Date date = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd/HH_mm_ss");
                String simDate = sdf.format(date);
                ParsePosition pos = new ParsePosition(0);
                Date nowdate = null;
                try {
                    nowdate = sdf.parse(simDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                System.out.println(simDate);
            }
        };
        timer.schedule(timerTask,0);
    }
}
