package com.precisionmedcare.jkkj.utils;

import com.alibaba.fastjson.JSON;

/**
 * Created by qilonghui on 20/03/26
 * API响应结果
 */
public class ResultAnother<T> {
    private int code;
    private String message;
    private T data;

    public ResultAnother setCode(Integer code) {
        this.code = code;
        return this;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public ResultAnother setMessage(String message) {
        this.message = message;
        return this;
    }

    public T getData() {
        return data;
    }

    public ResultAnother setData(T data) {
        this.data = data;
        return this;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }

    public static <T>  ResultAnother<T> fail(Integer code,T data) {
        ResultAnother<T> ret = new ResultAnother<T>();
        ret.setCode(code);
        ret.setData(data);
        return ret;
    }

    public static <T>  ResultAnother<T> failMessage(Integer code,String msg) {
        ResultAnother<T> ret = new ResultAnother<T>();
        ret.setCode(code);
        ret.setMessage(msg);
        return ret;
    }
    public static <T>  ResultAnother<T> successMessage(Integer code,String msg) {
        ResultAnother<T> ret = new ResultAnother<T>();
        ret.setCode(code);
        ret.setMessage(msg);
        return ret;
    }

    public static <T> ResultAnother<T> success(Integer code,T data) {
        ResultAnother<T> ret = new ResultAnother<T>();
        ret.setCode(code);
        ret.setData(data);
        return ret;
    }

}
