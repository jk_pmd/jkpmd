package com.precisionmedcare.jkkj.utils;

import javax.servlet.http.HttpSession;
import java.io.File;

public class UploadUtils {
    // 项目根路径下的目录  -- SpringBoot static 目录相当于是根路径下（SpringBoot 默认）
    public final static String IMG_PATH_PREFIX = "static/upload/image";
    public static String times;
    public static long fileTimes;
    public static boolean flag = false;
    public static int index = 0;
    /*
    *   创建或寻找项目下面的某个文件夹  没有则创建
    * */
    public static File getImgDirFile(String uploadPath,HttpSession session,String uuid){
       /* UploadUtils uploadUtils = new UploadUtils();
        long timeStap = uploadUtils.getTimeStap();*/
        times = uuid;
        String fileDirPath =  new String( uploadPath + File.separator + session.getAttribute("username") + File.separator + uuid);

        // 构建上传文件的存放 "文件夹" 路径
        /*if (timeStap != 0) {
//            fileDirPath = new String("src/main/resources/" + IMG_PATH_PREFIX + "/" + session.getAttribute("username") + "/" + timeStap);
            fileDirPath = new String( uploadPath + File.separator + session.getAttribute("username") + File.separator + timeStap);
        }else {
//            fileDirPath = new String("src/main/resources/" + IMG_PATH_PREFIX + "/" + session.getAttribute("username") + "/" + times);
            fileDirPath = new String(uploadPath + File.separator + session.getAttribute("username") + File.separator+ times);
            index += 1;
            if (index == 3) {
                flag = false;
                index = 0;
            }
        }*/
        File fileDir = new File(fileDirPath);
        if(!fileDir.exists()){
            // 递归生成文件夹
            fileDir.mkdirs();
        }
        return fileDir;
    }
   /* public long getTimeStap(){
        long l = 0;
        if(!flag){
            l = System.currentTimeMillis();
            times = l;
            flag = true;
            return l;
        }
        return l;
    }*/
    /*
    *  获取某个路径下面的文件，返回文件的绝对路径
    * relativePath:项目跟路径下的目录  --springboot static 目录相当于根路径（SpringBoot 默认）
    * */
    public static File getFileDirPath(String relativePath){
        String filePath = new String("src/main/resources/" + relativePath);
        File file = new File(filePath);
        return file;
    }


    /*
     *   创建或寻找项目下面的某个文件夹  没有则创建
     * */
    public static File getFileDir(String uploadPath,HttpSession session){

        fileTimes = System.currentTimeMillis();
        String fileDirPath = "";
        // 构建上传文件的存放 "文件夹" 路径
        fileDirPath = new String(uploadPath + File.separator + session.getAttribute("username") + File.separator+ fileTimes);
        File fileDir = new File(fileDirPath);
        if(!fileDir.exists()){
            // 递归生成文件夹
            fileDir.mkdirs();
        }
        return fileDir;
    }
}
