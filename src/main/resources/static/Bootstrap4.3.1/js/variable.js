let resultgrounid;
let patient_id;
let detect_id;
let result;
let last;
let errorInfo;
let isResult;

class Variable {
    constructor(resultgrounid, patient_id, detect_id, result, last, errorInfo, isResult) {
        this.resultgrounid = resultgrounid;
        this.patient_id = patient_id;
        this.detect_id = detect_id;
        this.result = result;
        this.last = last;
        this.errorInfo = errorInfo;
        this.isResult = isResult;
    }

    get getResultGrounId() {
        return this.resultgrounid;
    }

    get getPatientId() {
        return this.patient_id;
    }

    get getDetectId() {
        return this.detect_id;
    }

    get getResult() {
        return this.result;
    }

    get getLast() {
        return this.last;
    }

    get getErrorInfo() {
        return this.errorInfo;
    }

    get getIsResult() {
        return this.isResult;
    }


}