let urlCh;
let urlEn;
let localUser = localStorage.getItem("username");
function analysisResCh() {
    let urlCh = localStorage.getItem("urlCh");
    if (urlCh === undefined) {
        alert('请选择影像图片上传或查看历史记录');
        return false;
    }else if (username !== localUser) {
        alert('请选择影像图片上传或查看历史记录');
        return false;
    } else {
        window.location.href = urlCh;
    }
}
function analysisResEn() {
    let urlEn = localStorage.getItem("urlEn");
    if (urlEn === undefined) {
        alert('Please select the image to upload or view the history');
        return false;
    }else if (username !== localUser) {
        alert('Please select the image to upload or view the history');
        return false;
    } else {
        window.location.href = urlEn;
    }
}