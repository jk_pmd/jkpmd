//全局的ajax访问，处理ajax清求时session超时
$.ajaxSetup({
    contentType:"application/x-www-form-urlencoded;charset=utf-8",
    complete:function(XMLHttpRequest,textStatus){
        //通过XMLHttpRequest取得响应头，sessionstatus，
        var sessionstatus=XMLHttpRequest.getResponseHeader("SESSIONSTATUS");
        var url=XMLHttpRequest.getResponseHeader("CONTEXTPATH");
        if(sessionstatus=="TIMEOUT"){
            //如果超时就处理 ，指定要跳转的页面(比如登陆页)
            alert("身份已失效，请重新登录！");
            window.location.replace(url);
        }
    },
    //或者根据状态码判断
   /* statusCode: {
        403: function() {
            alert('数据获取/输入失败，没有此服务。403');
        },
        504: function() {
            alert('数据获取/输入失败，服务器没有响应。504');
        },
        500: function() {
            alert('服务器有误。500');
        }
    }*/
});

