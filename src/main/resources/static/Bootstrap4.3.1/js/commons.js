function Logout() {
   layer.msg('<span style="font-size:20px">确定退出本系统吗?</span>', {
        time: 0 //不自动关闭
        , btn: ['<span style="font-size:20px">确定</span>', '<span style="font-size:20px">取消</span>'],
           success: function(layero){
               layero.find('.layui-layer-btn').css('text-align', 'center')
           }
        , yes: function (index) {
            localStorage.clear();
           // localStorage.removeItem('urlCh');
           // localStorage.removeItem('urlEn');
           // localStorage.removeItem('username');
            window.location.href = "../../index.html";
        }
    });
}
function LogoutEnglish() {
    layer.msg('<span style="font-size:20px">Are you sure quit this system?</span>', {
        time: 0 //不自动关闭
        , btn: ['<span style="font-size:20px">yes</span>', '<span style="font-size:20px">cancel</span>'],
            success: function(layero){
                layero.find('.layui-layer-btn').css('text-align', 'center')
            }
        , yes: function (index) {
            localStorage.clear();
            // localStorage.removeItem('urlCh');
            // localStorage.removeItem('urlEn');
            // localStorage.removeItem('username');
            window.location.href = "../../index.html";
        }
    });
}
const updatePwd = () => {
    layer.open({
        type: 2,
        title: '密码修改',
        maxmin: true,
        area: ['450px', '450px'],
        content: '../updatePwd/updatePwd.html?' + 'username=' + username,
        end: function () {
            layer.tips('Hi', '#about', {tips: 1})
        },
    });
};
const updatePwdEnglish = () => {
    layer.open({
        type: 2,
        title: 'change Password',
        maxmin: true,
        area: ['450px', '450px'],
        content: '../updatePwd/updatePwdEnglish.html?' + 'username=' + username,
        end: function () {
            layer.tips('Hi', '#about', {tips: 1})
        },
    });
};

function createCode() {
    $.post("../login/insertLoginCode", {}, function (data) {
        if(data.success){
            layer.msg("已成功生成激活码",{icon: 1});
        }else {
            layer.msg("激活码生成失败",{icon: 2});
        }
    });
}

/*let html = '&nbsp;&nbsp;&nbsp;&nbsp;<span>精康检测卡号：</span><input id="ICInput" type="text" class="layui-layer-input" disabled/><br>' +
    '&nbsp;&nbsp;&nbsp;&nbsp;<span>重置检测卡号：</span><input id="RestInput" type="text" class="layui-layer-input" autocomplete="off"/><br><div style="padding: 10px 15px 12px">' +
    '<button id="save" style="margin-left: 32.5%;font-size: 14px;border-color: #1E9FFF;background-color: #1E9FFF;color: #fff">重置</button>&nbsp;&nbsp;&nbsp;' +
    '<button id="quit" style="font-size: 14px">关闭</button></div>' +
    '<script>' +
    '$("#ICInput").val($("#icKA").val());' +
    '$("#save").click(function() {' +
    'api.saveTempNumber($("#ICInput").val(),$("#RestInput").val()).then((res) => {\n' +
    '                    if (res.success === true && res.content === 1) {\n' +
    '                        layer.msg("操作成功");\n' +
    '                        $("#icKA").val($("#RestInput").val());\n' +
    '                        localStorage.setItem("icKA", $("#RestInput").val());\n' +
    '                    } else if (res.success===false && res.content === 0) {\n' +
    '                        layer.msg("重置精康检测卡号必须先在精康检测卡读取位置刷卡，再重置。");\n' +
    '                    }else if (res.success===false && res.content === "") {\n' +
    '                        layer.msg("something error！");\n' +
    '                    }\n' +
    '                });' +
    '});' +
    '$("#quit").click(function() { layer.closeAll(); })' +
    '</script>';*/

let html = '&nbsp;&nbsp;&nbsp;&nbsp;精康检测卡号：<input id="ICInput" type="text" class="layui-layer-input" disabled/><br>' +
    '&nbsp;&nbsp;&nbsp;&nbsp;自定义的卡号：<input id="RestInput" type="text" class="layui-layer-input" autocomplete="off"/><br>' +
    '<script>' +
    '$("#ICInput").val($("#icKA").val());' +
    '</script>';
let htmlEnglish = '&nbsp;&nbsp;&nbsp;&nbsp;JK card number：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="ICInput" type="text" class="layui-layer-input" disabled/><br>' +
    '&nbsp;&nbsp;&nbsp;&nbsp;Custom card number：<input id="RestInput" type="text" class="layui-layer-input" autocomplete="off"/><br>' +
    '<script>' +
    '$("#ICInput").val($("#icKA").val());' +
    '</script>';

function renderSize(value) {
    if (null == value || value == '') {
        return "0 Bytes";
    }
    let unitArr = new Array("Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB");
    let index = 0;
    let srcsize = parseFloat(value);
    index = Math.floor(Math.log(srcsize) / Math.log(1024));
    let size = srcsize / Math.pow(1024, index);
    size = size.toFixed(2);//保留的小数位数
    return size + unitArr[index];
}

/*
function bytesToSize(bytes) {
    if (bytes === 0) return '0 B';
    var k = 1024;
    sizes = ['B','KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    i = Math.floor(Math.log(bytes) / Math.log(k));
    return (bytes / Math.pow(k, i)) + ' ' + sizes[i];
    //toPrecision(3) 后面保留一位小数，如1.0GB
    //return (bytes / Math.pow(k, i)).toPrecision(3) + ' ' + sizes[i];
}*/
function getCurrentDate(date){
    let y = date.getFullYear();
    let m = date.getMonth()+1;
    let d = date.getDate();
    let h = date.getHours();
    let min = date.getMinutes();
    let s = date.getSeconds();
    return y + '-' + (m < 10 ? ('0' + m) : m) + '-' + (d < 10 ? ('0' + d) : d) + ' ' + (h < 10 ? ('0' + h) : h) + ':' + (min < 10 ? ('0' + min) : min) + ':' + (s < 10 ? ('0' + s) : s);
}