create table discernlog
(
    id          int auto_increment
        primary key,
    upTime      datetime      null,
    tempPath    varchar(300)  null,
    upUser      varchar(50)   null,
    detectionId varchar(100)  null,
    patientId   varchar(100)  null comment '识别记录',
    isResult    int default 0 null
)
    charset = utf8;

create table logincode
(
    id         int auto_increment   primary key,
    activecode varchar(100)         not null,
    status     tinyint(1) default 1 null,
    userid     int                  null
);

create table loginlog
(
    id      int auto_increment
        primary key,
    userId  varchar(50) null,
    logTime datetime    null,
    logType int         null
)
    charset = utf8;

create table loginuser
(
    ID       int auto_increment
        primary key,
    pwd      varchar(100)         null,
    ugroup   varchar(100)         null,
    remarks  varchar(100)         null,
    type     int        default 1 null comment ' 1 普通， 2管理用户',
    endTime  date                 null comment ' 到期时间',
    isDel    tinyint(1) default 0 null,
    userName varchar(100)         null
)
    charset = utf8;

create table resulterror
(
    id          int auto_increment
        primary key,
    upUser      varchar(50)  null comment '暂时弃用',
    upTime      datetime     null,
    detectionId varchar(100) null,
    patientId   varchar(100) null,
    groupId     int          null,
    errInfo     varchar(100) null
)
    charset = utf8;

create table resultgroup
(
    id          int auto_increment
        primary key,
    upUser      varchar(50)          null,
    detectionId varchar(100)         null,
    patientId   varchar(100)         null,
    upTime      datetime             null,
    isResult    int        default 0 null comment '是否计算过结果',
    reResult    varchar(100)         null,
    reText      varchar(200)         null,
    doctorNotes varchar(200)         null,
    errInfo     varchar(500)         null,
    isErr       tinyint(1) default 0 not null,
    ip          varchar(50)          null,
    date        varchar(50)          null,
    city        varchar(50)          null,
    size        float(10, 2)         null,
    isDel       tinyint(1) default 0 not null
)
    charset = utf8;

create table resultlog
(
    id          int auto_increment
        primary key,
    upUser      varchar(50)   null,
    upTime      datetime      null,
    detectionId varchar(100)  null,
    patientId   varchar(100)  null,
    groupId     int           null,
    imgPath     varchar(300)  null comment '图片相对地址',
    reResult    varchar(100)  null comment '结果',
    reSpace     varchar(100)  null comment '置信区间',
    reText      varchar(200)  null comment '结论',
    isRe        int default 0 null comment '是否计算过结果'
)
    charset = utf8;

create table temppwd
(
    id         int auto_increment
        primary key,
    userId     varchar(50)   null,
    staticTime datetime      null,
    staticPwd  varchar(100)  null,
    isUse      int default 0 null
)
    charset = utf8;

create table iccard
(
    id     int auto_increment
        primary key,
    number varchar(18)     not null,
    count  int default 100 not null,
    tempnumber varchar(50) not null
)
    charset = utf8;